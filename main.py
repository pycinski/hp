# -*- coding: utf-8 -*-

import glob
import itertools
import os
import sys
import shutil
import numpy as np
import logging
import time


ti2=''   #TODO poprawić; zmienna globalna do pomiaru czasu; niepoprawnie, ale działa

def pomiar_czasu(last, comment):
    current = time.time()
    elapsed = current - last
    logging.warning(comment+ " " + str(elapsed))
    return current

# The values need to be set before cv2 is loaded!
os.environ["OPENCV_IO_MAX_IMAGE_HEIGHT"] = str(1000000)
os.environ["OPENCV_IO_MAX_IMAGE_WIDTH"] = str(1000000)
os.environ["OPENCV_IO_MAX_IMAGE_PIXELS"] = str(1000000 * 1000000)
import cv2

import file_reading as fr
import transformations as tr
import image_processing as ip
import project_config
import register_normalized

# TODO:
# - suppress output (convert print to logging.xxx)
# - output current stage
# - add missing pydoc
# - if FIJI registration fails, interrupt whole process
# - maybe increase "Geometric Consensus Filter - maximal alignment error"


# import scripts

# Left roi chosen by AG, coordinates fit to scale 1/2
roiL_x2 = (7987, 8985)
roiL_y2 = (16930, 17814)
# make sure that ROIs have five smallest bits zeroed
# (because it is in scale 1/2. Therefore we can downsize up to 1/64 (5 bits))
# extend lower-rigt pixel by one
roiL_x2 = (roiL_x2[0] & 0xffe0, (roiL_x2[1] & 0xffe0) + 0x0020)
roiL_y2 = (roiL_y2[0] & 0xffe0, (roiL_y2[1] & 0xffe0) + 0x0020)
# divide ROI coords by 32 (from 1/2 to 1/64)
roiL_x64 = (roiL_x2[0] >> 5, roiL_x2[1] >> 5)
roiL_y64 = (roiL_y2[0] >> 5, roiL_y2[1] >> 5)
roiL_64 = list(itertools.product(roiL_x64, roiL_y64))


# Right roi chosen by AG, coordinates fit to scale 1/2
roiR_x2 = (10537, 11521)
roiR_y2 = (14565, 15575)
roiR_x2 = (roiR_x2[0] & 0xffe0, (roiR_x2[1] & 0xffe0) + 0x0020)
roiR_y2 = (roiR_y2[0] & 0xffe0, (roiR_y2[1] & 0xffe0) + 0x0020)
roiR_x64 = (roiR_x2[0] >> 5, roiR_x2[1] >> 5)
roiR_y64 = (roiR_y2[0] >> 5, roiR_y2[1] >> 5)
roiR_64 = list(itertools.product(roiR_x64, roiR_y64))


# new roi in arbitrary position
new_roi_x64 = (250, 250 + 30)
new_roi_y64 = (100, 100 + 30)
new_roi_64 = list(itertools.product(new_roi_x64, new_roi_y64))


# ROI-3 dimensions:
# 1/64:  (24x32+250+266)

# ROI-4 dimensions:
# 1/64:  (35x35+255+255)



def transform_rois_64_and_crop_16(config, origFilenames, transformParams):
    transformed_bounding_boxes = []
    roi = config.roi_scale_64

    input_dir_16 = config.base_dir + config.resized_subdir + "16/"

    for origImageFname, transformation in zip(origFilenames, transformParams):
        print('orig 64; transformation ', origImageFname, transformation)
        transformation = tr.inverseTransformation(transformation)

        transformed_roi = tr.transformROI(*roi, transformation)

        # enlarge roi by defined factor and compute enlarged bb
        enlarged_roi = tr.scaleRoiByFactor(transformed_roi, config.roi_coeficient_x16)

        bb = tr.computeOneBoundingBoxFromROI(enlarged_roi)
        bb_center = 0.5 * np.array((bb[2] + bb[0], bb[3] + bb[1]))
        # roi[3] - prawy dół;   roi[0] - lewa góra
        size = (np.array(roi[3]) - np.array(roi[0])) * config.roi_coeficient_x16
        bb2 = np.append(bb_center - 0.5 * size, bb_center + 0.5 * size)

        print(bb2)

        # apply bb dimension to images x16
        bb16 = [int(x) << 2 for x in bb2]
        transformed_bounding_boxes.append(bb16)

        ifname = input_dir_16 + origImageFname
        print('ifname 16: ', ifname)
        fullImage = fr.openImageFile(ifname)
        cropped_image = fullImage.crop(bb16)
        ofname = ifname.replace(config.resized_subdir,  config.output_dir + config.cropped_subdir)
        print('ofname 16: ', ofname)
        cropped_image.save(ofname)

        '''roi2 = [x << 5 for x in roi]
        ifname = origImageFname.replace('/64/', '/2/')
        print('ifname: ', ifname)
        fullImage = fr.openImageFile(ifname)
        croppedImage = fullImage.crop(roi2)
        ofname = ifname.replace('/resized/', '/cropped/')
        print('ofname: ', ofname)
        croppedImage.save(ofname)'''
    return transformed_bounding_boxes


def transform_rois_16_and_crop_1(config, cropped16Filenames, transform16Params, bbs16):
    transformed_bounding_boxes = []
    cropped_dir_16 = config.base_dir + config.output_dir + config.cropped_subdir + "16/"
    input_dir_1 = config.base_dir + config.original_subdir

    for filename, transformation, bb in zip(cropped16Filenames, transform16Params, bbs16):
        transformation = tr.inverseTransformation(transformation)

        # size of all image
        size = np.array([bb[2] - bb[0], bb[3] - bb[1]])
        corners = itertools.product((0, size[0] - 1), (0, size[1] - 1))

        new_corners = [tr.transformPoint(p, transformation) for p in corners]

        new_center = np.mean(np.array(new_corners), axis=0)
        new_bb = np.append(new_center - 0.5 * size, new_center + 0.5 * size)
        bb_with_offset = new_bb + (bb[0], bb[1], bb[0], bb[1])

        if False:
            # crop from x16, for Testing only
            # WARN the following paths were not checked
            ifname = cropped_dir_16 + filename
            fullImage = fr.openImageFile(ifname)
            cropped_image = fullImage.crop(bb_with_offset)
            ofname = ifname.replace(conf.resized_subdir,  config.output_dir + conf.cropped_subdir)
            ofname += "-test.tif"
            cropped_image.save(ofname)

        # additionally decrease ROI by some factor
        bb_finally_resized = tr.scaleBBByFactor(bb_with_offset, (1.0 / config.roi_coeficient_x16) / config.decrease_param)
        # apply bb dimension to images x16
        bb1 = tuple(int(x) << 4 for x in bb_finally_resized)
        transformed_bounding_boxes.append(bb1)
        print(bb1)
        ifname = config.base_dir + config.original_subdir + filename
        print('ifname 1 : ', ifname)

        #WARNING cannot open ifname by PIL. Probably image is too large
        fullImage_data = cv2.imread(ifname)
        # watch out, cv swaps X and Y axis! Moreover, it works in BGR, not RGB, so better do not mix them
        cropped_data = fullImage_data[bb1[1]:bb1[3], bb1[0]:bb1[2], :]
        ofname = cropped_dir_16.replace('/16/', '/1/') + filename
        print('ofname 1 : ', ofname)
        cv2.imwrite(ofname, cropped_data)

    return transformed_bounding_boxes


# noinspection PyPep8Naming
def main(config):
    ti3 = pomiar_czasu(ti2, "START funkcji main!")
    transformParams = ip.register_images_by_fiji(config, 64, is_cropped=False, use_previous_results=False)
    ti4 = pomiar_czasu(ti3, "rejestracja obrazów x64!")
    full_orig_filenames = fr.getFilesFromDir(config.base_dir + config.resized_subdir + "64", "*.tif")
    image_filenames = [os.path.basename(path) for path in full_orig_filenames]
    roi = config.roi_scale_64
    print('roi: {}'.format(roi))

    ti5 = pomiar_czasu(ti4, "przed transformacją")
    bbs16 = transform_rois_64_and_crop_16(config, image_filenames, transformParams)
    ti6 = pomiar_czasu(ti5, "crop obrazów x16")

    print('bbs16', bbs16)

    transform16Params = ip.register_images_by_fiji(config, 16, is_cropped=True, use_previous_results=False)
    ti7 = pomiar_czasu(ti6, "rejestracja cropów x16")
    # cropped16Filepaths = fr.getFilesFromDir(config.base_dir + config.output_dir + config.cropped_subdir + "16", "*.tif")
    # cropped16Filenames = [os.path.basename(path) for path in cropped16Filepaths]
    bbs1 = transform_rois_16_and_crop_1(config, image_filenames, transform16Params, bbs16)
    ti8 = pomiar_czasu(ti7, "crop obrazów x1")

    input_directory_to_deconvolute = config.base_dir + config.output_dir + config.cropped_subdir + "1"
    ip.deconvolute_and_save_cropped_images(config, input_directory_to_deconvolute, image_filenames)
    ti9 = pomiar_czasu(ti8, "dekonwolucja cropów x1")
    # TODO add some debug logging
    ip.register_images_by_fiji(config, 1, channel="hematoxyline", is_cropped=True)
    ti10 = pomiar_czasu(ti9, "rejestracja obrazów x1 kanał H")

    ip.register_images_by_fiji(config, 1, is_cropped=True)
    ip.transform_images_by_fiji(config, 1, is_cropped=True)
    ti11 = pomiar_czasu(ti10, "transformacja obrazów x1")

    input_directory_to_deconvolute = config.base_dir + config.output_dir + config.cropped_registered_fiji_subdir + "1" + config.transformed_channel_dir_suffix
    ip.deconvolute_and_save_cropped_images(config, input_directory_to_deconvolute, image_filenames)
    ti12 = pomiar_czasu(ti11, "kolejna dekonwolucja i zapis")

    for filename in fr.getFilesFromDir(config.base_dir + config.output_dir + config.cropped_registered_fiji_subdir+"1" + config.transformed_channel_dir_suffix, "*.tif"):
        #TODO change basename of final images
        ip.add_alpha_to_image(filename, filename+'-rgba.tif')
        ip.add_alpha_to_image_based_on_hemato(filename, filename + '-rgba-h.tif')

    ti13 = pomiar_czasu(ti12, "dodanie alfy do obrazów")
    # ip.crop_black_borders_of_images(config.base_dir+config.cropped_registered_fiji_subdir+"1", "*.tif")

    # WARNING: for ofname in $(ofname dir); do convert $ofname $ofname.png; done

'''
        #croppedImage = bigImage.crop(roi)
        #rotatedImage = croppedImage
        #if alpha != 0:
        #    rotatedImage = croppedImage.rotate(alpha * 180.0 / np.pi, resample=Image.NEAREST, expand=True,
        #                                       fillcolor=0xffffff)
        #    # TODO: cut the image's borders after the rotation... Or not? Do it only in final iteration!
        #rotatedImage.save(os.path.dirname(transformedFname)+"-crop/" + os.path.basename(transformedFname))
'''


# how to iterate over corners coordinates
# for point in (itertools.product((roi_x_min, roi_x_max), (roi_y_min, roi_y_max))):
#    newPoint = tr.transformPoint(point, transformation)
# print (roi)


if __name__ == '__main__':

    poczatek_czasu =  time.time()
    ti1 = pomiar_czasu(poczatek_czasu, "START!")
    conf = project_config.Config()
    conf.set_logging()

#    conf.roi_scale_64 = new_roi_64
#    conf.roi_scale_64 = roiR_64 
#    conf.output_dir += "_out" + '/'

    #conf.fiji_exe = ""

#    conf.base_dir = '/media/bapy/Samsung_T5/hp/adenoca/'
#    conf.base_dir += "test/"
#    #conf.base_dir = "/home/bapy/projects/la/hp/test/"
#    conf.resized_subdir = "resized/"
#    conf.original_subdir = "original/"

    if len(sys.argv) > 1:
        logging.warning("Parsing command line arguments as: x, y, width, height")
        x = int(sys.argv[1])
        y = int(sys.argv[2])
        width = int(sys.argv[3])
        height = int(sys.argv[4])
        conf.roi_scale_64 = list(itertools.product((x, x+width), (y, y+height)))
        conf.output_dir += "roi-" + str(x) + "_" + str(y) + '/'


    #TODO does not work (shutil.rmtree('./test/./') )
    #conf.clean_output_tree()
    conf.set_and_create_new_output_tree(conf.output_dir)

    #ip.transform_images_by_fiji(conf, 1, True)
    #sys.exit(5)

    
    ti2 = pomiar_czasu(ti1, "START!")
    main(conf)
    # pass
    # ip.runFijiRegistration()
