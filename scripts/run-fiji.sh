
FIJI_EXE="/home/mba/programy/Fiji.app/ImageJ-linux64"
SCRIPT=hello.py
SCRIPT=register-images.py

# source directory
SOURCE_DIR="/home/mba/projects/hp/data/mrhead/orig/"
# output directory
TARGET_DIR="/home/mba/projects/hp/data/mrhead/out/"
# transforms directory
TRANSF_DIR="/home/mba/projects/hp/data/mrhead/out/"
# reference image
REFERENCE_NAME="mrhead01.png"

"$FIJI_EXE" --ij2 --headless --console --run  $SCRIPT  "source_dir=\"${SOURCE_DIR}\",target_dir=\"${TARGET_DIR}\",transf_dir=\"${TRANSF_DIR}\",reference_name=\"${REFERENCE_NAME}\"" 

# 'source_dir="/home/mba/projects/hp/data/mrhead/orig/",target_dir="/home/mba/projects/hp/data/mrhead/out/",transf_dir="/home/mba/projects/hp/data/mrhead/out/",reference_name="mrhead01.png"'

#'source_dir="abc",target_dir="def",transf_dir="xyz",reference_name="qwe"'
