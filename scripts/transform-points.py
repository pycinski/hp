import glob
import itertools
import logging
import os
import sys
import file_reading as fr
import image_processing as ip
import transformations as tr
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as ss



logging.basicConfig(level=logging.DEBUG)

ROOT_DIRECTORY = '/home/bapy/data/color-normalized'

def compute_distances(roi: int, global_normalisation_method: int, local_normalisation_method: int):
    results = []
    coordinates = []
    xml_input_directory = ROOT_DIRECTORY+f'/normalized/Color_normalized_{global_normalisation_method}/roi-{roi}/{local_normalisation_method}-h-registered'
    point_coords_filename = ROOT_DIRECTORY+f"/roi-{roi}_coords.csv"

    if global_normalisation_method < 0 and local_normalisation_method < 0:
        xml_input_directory = f'/home/bapy/data/color-normalized/before_normalisation/original/roi-{roi}/1-h-registered'

    xml_filenames = fr.getFilesFromDir(xml_input_directory, "*.xml")
    transforms = fr.parseXmlsWithTransformResults(xml_filenames)
    points = pd.read_csv(point_coords_filename, header=None, sep=" ", dtype='float').to_numpy()

    for i in range(1, len(transforms)):
        current_point = np.array([points[i, 0], points[i, 2]])
        previous_point = np.array([points[i-1, 1], points[i-1, 3]])

        previous_transform = transforms[i-1]
        current_transform = transforms[i]

        new_previous_point = np.array(tr.transformPoint(previous_point, previous_transform))
        new_current_point = np.array(tr.transformPoint(current_point, current_transform))

        distance = np.linalg.norm(new_previous_point - new_current_point)
        distance_vector = new_previous_point - new_current_point
        # 9th point is an outlier
        if roi == 1 and i == 9:
            continue
        results.append(distance)
        #coordinates.append([new_previous_point.tolist(), new_current_point.tolist()])
        coordinates.append(distance_vector.tolist())
        
        #print(i, new_previous_point, new_current_point, current_point, distance)

    return results, coordinates


def compute_ATRE(coordinates):
    results = [[],[],[]]
    for roi in [0, 1, 2]:
        current_results = results[roi]
        for normalisation_approach in range(15):
            coords = coordinates[roi][normalisation_approach]
            
            cummulative_vector = np.cumsum(coords, axis=0)
            cummulative_error = [np.linalg.norm(a) for a in cummulative_vector]
            current_results.append(cummulative_error)
    return results


distances = [[], [], []]
coordinates = [[], [], []]

for roi in [1, 2, 3]:
    distances.append(list())
    dist, coords = compute_distances(roi, -1, -1)
    distances[roi-1].append(dist)
    coordinates[roi-1].append(coords)
    for global_normalisation_method in [1, 2]:
        for local_normalisation_method in range(1, 7+1):
            dist, coords = compute_distances(roi, global_normalisation_method, local_normalisation_method)
            distances[roi-1].append(dist)
            coordinates[roi-1].append(coords)

atre = compute_ATRE(coordinates)

atre_roi1 = np.array(atre[0])
atre_roi2 = np.array(atre[1])
atre_roi3 = np.array(atre[2])

# %% Save results
OUTPUT_DIRECTORY = 'data'
pd.DataFrame(atre_roi1).to_csv(OUTPUT_DIRECTORY+'/atre1.csv', sep=' ', header=False, index=False)
pd.DataFrame(atre_roi2).to_csv(OUTPUT_DIRECTORY+'/atre2.csv', sep=' ', header=False, index=False)
pd.DataFrame(atre_roi3).to_csv(OUTPUT_DIRECTORY+'/atre3.csv', sep=' ', header=False, index=False)

# %% Save results
pd.DataFrame(np.array(distances[0])).to_csv(OUTPUT_DIRECTORY+'/distances1.csv', sep=' ', header=False, index=False)
pd.DataFrame(np.array(distances[1])).to_csv(OUTPUT_DIRECTORY+'/distances2.csv', sep=' ', header=False, index=False)
pd.DataFrame(np.array(distances[2])).to_csv(OUTPUT_DIRECTORY+'/distances3.csv', sep=' ', header=False, index=False)


