#!/bin/bash

# a script for resizing all original tiff files
# WARNING! a single command `convert` lasts at least 16s, consumes over 13GB of RAM
# one need to disable ImageMagick policy to be able to process such large files!
# i.e. comment completely all captcha from /etc/ImageMagick-6/policy.xml 
# i.e. policy containing: width, memory, disk etc.


echo WARNING: no file or directory may contain spaces!  >&2

INPUT_DIR="Tiled_TIFF"

mkdir -p resized/2
mkdir -p resized/4
mkdir -p resized/8
mkdir -p resized/16
mkdir -p resized/32
mkdir -p resized/64


ls $INPUT_DIR | while read f; do
convert $INPUT_DIR/$f  -sample "1.5625%"  "resized/64/$f" 
convert $INPUT_DIR/$f  -sample "3.125%"  "resized/32/$f" 
convert $INPUT_DIR/$f  -sample "6.25%"  "resized/16/$f" 
convert $INPUT_DIR/$f  -sample "12.5%"  "resized/8/$f" 
convert $INPUT_DIR/$f  -sample "25%"  "resized/4/$f" 
convert $INPUT_DIR/$f  -sample "50%"  "resized/2/$f" 
done

