import glob
import itertools
import logging
import os
import sys
import file_reading as fr
import image_processing as ip
import transformations as tr
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as ss

ROOT_DIRECTORY = 'data'

pd.set_option('display.max_columns', 500)
pd.set_option('precision', 2)


# TODO The following function should be refactored: split into changing normality, then do appropriate tests.


def compute_statistics_normal_distribution(roi_nr, filepath_csv, name, print_tests=True, print_table=True):
    print(f"# Compute statistics of {name} for ROI {roi_nr}")
    df = pd.DataFrame(pd.read_csv(filepath_csv, header=None, sep=",", dtype='float').to_numpy().T)
    description = df.describe(include='all')
    indices = ['mean', 'std']
    description = description.loc[indices]

    if print_table:
        print(description.to_latex())

    if print_tests:
        print(f'Test for {name} normality')
        for method_id in range(8):
            print(method_id, 'ShapiroResult(pvalue='+str(ss.shapiro(df[method_id]))+')')
        for method_id in range(1, 8):
            print(method_id, ss.ttest_rel(df[0], df[method_id]))  #parę razy jest wynik>5%...


def compute_statistics_non_normal(roi_nr, filepath_csv, name, print_tests=True, print_table=True):
    print(f"# {name} ROI-{roi_nr} Stats")
    roi_results = pd.read_csv(filepath_csv, sep=' ', header=None, dtype='float').to_numpy()
    df = pd.DataFrame(roi_results[0:8].T)
    description = df.describe(include='all')
    description = description.append(pd.Series((description.loc['75%']-description.loc['25%'])/2.0, name='QD'))
    description = description.rename(index={'50%': 'median'})
    indices = ['median', 'QD']
    description = description.loc[indices]

    if print_table:
        print(description.to_latex())

    if print_tests:
        print('Test for normality')
        for method_id in range(8):
            print(method_id, 'ShapiroResult(pvalue='+str(ss.shapiro(roi_results[method_id]))+')')
        print("## Compare all in one")
        print(ss.friedmanchisquare(*roi_results[0:8]))

        print("## Compare original to consequent normalisation methods")
        # #We do not use normalization 8-14 anymore
        for i in range(1, 14 - 7 + 1):
            print(ss.wilcoxon(roi_results[0, :], roi_results[i, :]))
        # #We do not use normalization 8-14 anymore
        # print("## Compare ''method 1'' to ''method 2''")
        # for i in range(1, 7 + 1):
        #    print(ss.wilcoxon(roi_results[i, :], roi_results[i + 7, :]))


# compute_statistics_non_normal(1, ROOT_DIRECTORY+'/distances1.csv', "TRE", print_table=False)
# compute_statistics_non_normal(2, ROOT_DIRECTORY+'/distances2.csv', "TRE", print_table=False)
# compute_statistics_non_normal(3, ROOT_DIRECTORY+'/distances3.csv', "TRE", print_table=False)

compute_statistics_non_normal(1, ROOT_DIRECTORY + '/atre1.csv', "ATRE", print_table=False)
compute_statistics_non_normal(2, ROOT_DIRECTORY + '/atre2.csv', "ATRE", print_table=False)
compute_statistics_non_normal(3, ROOT_DIRECTORY + '/atre3.csv', "ATRE", print_table=False)

# compute_statistics_normal_distribution(1, ROOT_DIRECTORY+"/jac-roi1.csv", "Jaccard", print_table=False)
# compute_statistics_normal_distribution(2, ROOT_DIRECTORY+"/jac-roi2.csv", "Jaccard", print_table=False)
# compute_statistics_normal_distribution(3, ROOT_DIRECTORY+"/jac-roi3.csv", "Jaccard", print_table=False)


# compute_statistics_normal_distribution(1, ROOT_DIRECTORY + "/rmse-roi1.csv", "RMSE", print_table=False)
# compute_statistics_normal_distribution(2, ROOT_DIRECTORY + "/rmse-roi2.csv", "RMSE", print_table=False)
# compute_statistics_normal_distribution(3, ROOT_DIRECTORY + "/rmse-roi3.csv", "RMSE", print_table=False)
