"""
rename files like DigitalSlide01_Wholeslide_Default_Extended.tif
"""

import os

files = [f for f in os.listdir('.') if f.startswith('DigitalSlide')]
for f in files:
    os.rename(f, "DS"+f[12:14]+".tif")
