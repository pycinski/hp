


for color_normalisation_method = 1:2
     input_base_dir = ['/media/bapy/6C1C4F661C4F2B02/results-color-normalized-before-croping/Color_normalized_' int2str(color_normalisation_method)];
    for roi_nr = 1:4
        % Tutaj wczytaj pierwszy obrazek
        input_roi_dir = [ input_base_dir  '/roi-' int2str(roi_nr)];
        im_roi = imread([ input_roi_dir '/1-registered/DS01_1.tif'] );
        im_roi = im_roi(:, :, 1) ~= 0;
        im_roi = ~bwareaopen(~im_roi, 1000);
        for color_nr = 1:7 
            for transformation_method = 1:2
                disp (color_nr);
                if (transformation_method==1)
                    full_dir = [input_roi_dir '/' int2str(color_nr) '-registered'];
                else
                    full_dir = [input_roi_dir '/' int2str(color_nr) '-transformed'];
                end
                im = imread([ full_dir '/DS01_' int2str(color_nr) '.tif'] );
                im = im(:, :, 1) ~= 0;
                im = ~bwareaopen(~im, 1000);
                filelist = dir(full_dir);
                sprintf(full_dir);
                for file_nr = 3:length(filelist)
                    fullFileName = fullfile(full_dir, filelist(file_nr).name);
                    im_rgb = imread(fullFileName);
                    im_bw = im_rgb(:, :, 1) ~= 0;
                    im_bw = ~bwareaopen(~im_bw, 1000);
                    im = im & im_bw;
                end
                % złącz maski różnych wymiarów
                max_size = max(size(im_roi), size(im));
                im = padarray(im, max([0,0], max_size - size(im)), 'post');
                im_roi = padarray(im_roi, max([0,0], max_size - size(im_roi)), 'post');
                im_roi = im_roi & im;
            end
        end
        %
        disp ('End of computing')

        lr = LargestRectangle(im_roi, 0, 0, 0, 0, 1);
        rect = [lr(2,1), lr(2,2), lr(4,1)-lr(2,1), lr(4,2)-lr(2,2)];

        RectangleDimsFileName = [ input_roi_dir '/rectangle.txt' ];
        RectangleDimsFileName = strrep(RectangleDimsFileName, '/home/bapy', '/media/bapy/6C1C4F661C4F2B02/hp_data');
        fid = fopen(RectangleDimsFileName, 'w');
        fprintf(fid, '%d %d %d %d\n', lr(2,1), lr(2,2), lr(4,1)-lr(2,1), lr(4,2)-lr(2,2) );
        fclose(fid);

        % Iteruj jeszcze raz po katalogach żeby wyciąć
        for color_nr = 1:7
            for transformation_method = 1:2
                if (transformation_method==1)
                    full_dir = [input_roi_dir '/' int2str(color_nr) '-registered'];
                else
                    full_dir = [input_roi_dir '/' int2str(color_nr) '-transformed'];
                end
                filelist = dir(full_dir);
                sprintf(full_dir);
                for file_nr = 3:length(filelist)
                    fullFileName = fullfile(full_dir, filelist(file_nr).name);
                    if (contains(fullFileName, 'rgba'))
                        continue
                    end
                    im_rgb = imread(fullFileName);
                    J = imcrop(im_rgb, rect);
                    
                    outputFileName = strrep(fullFileName, '/home/bapy', '/media/bapy/6C1C4F661C4F2B02/hp_data');
                    imwrite(J, outputFileName); 

                end
            end
        end
        
    end
end


