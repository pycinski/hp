#@String source_dir 
#@String target_dir 
#@String transf_dir 
#@String reference_name 

from register_virtual_stack import Register_Virtual_Stack_MT
 
# shrinkage option (false)
use_shrinking_constraint = 0
 
p = Register_Virtual_Stack_MT.Param()
# The "maximum image size":
#p.sift.maxOctaveSize = 1024
# The "inlier ratio":
#p.minInlierRatio = 0.05
p.registrationModelIndex = 1    #  0=TRANSLATION, 1=RIGID, 2=SIMILARITY, 3=AFFINE, 4=ELASTIC, 5=MOVING_LEAST_SQUARES
p.interpolate = False
 
print(source_dir, target_dir, transf_dir, reference_name)


Register_Virtual_Stack_MT.exec(source_dir, target_dir, transf_dir, reference_name, p, use_shrinking_constraint)
print("END_OF_FIJI_STACK_PROCESSING")
