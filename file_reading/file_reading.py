import os
from PIL import Image
import glob
from xmljson import badgerfish as bf
from xml.etree.ElementTree import fromstring
import logging

Image.MAX_IMAGE_PIXELS = 40000*60000


# noinspection PyPep8Naming
def openImageFile(filename):
    img = Image.open(filename)
    return img


# noinspection PyPep8Naming
def saveImageToFile(image, filename):
    image.save(filename)


# noinspection PyPep8Naming
def saveImagesToDir(imageList, outFilenames, outputDirectory):
    for image, filename in zip(imageList, outFilenames):
        saveImageToFile(image, outputDirectory+'/'+filename)


# noinspection PyPep8Naming
def getFilesFromDir(path, pattern="*"):
    return sorted(glob.glob(path+'/'+pattern))


# noinspection PyPep8Naming
def cropImage(image, geometry):
    if isinstance(image, str):
        image = openImageFile(image)
    if len(geometry) != 4:
        raise AttributeError("Geometry should have 4 elements")
    cropped_img = image.crop(geometry)
    return cropped_img


# noinspection PyPep8Naming
def cropListOfImages(imageList, geometry):
    croppedImages = [cropImage(image, geometry)for image in imageList]
    return croppedImages


# noinspection PyPep8Naming
def parseXmlsWithTransformResults(filenames):
    """
    Read XML files from FIJI's RegisterVirtualStack plugin.
    :param filenames: list of XML files
    :return: a list of tuples (x_transform, y_transform, angle)
    """
    result = []
    for filename in filenames:
        with open(filename) as f:
            data = bf.data(fromstring(f.read()))  # bf.data(x,root=)
            x_combined = 0.0
            y_combined = 0.0
            angle = 0.0
            try:
                x = 0.0
                y = 0.0
                angle = 0.0
                for key in data['ict_transform_list']['iict_transform']:
                    params = key['@data']
                    if key['@class'] == 'mpicbg.trakem2.transform.RigidModel2D':
                        (angle, x, y) = [float(val) for val in params.split(' ')]
                    elif key['@class'] == 'mpicbg.trakem2.transform.TranslationModel2D':
                        (x, y) = [float(val) for val in params.split(' ')]
                    x_combined += x
                    y_combined += y
            except KeyError:
                if data['iict_transform']['@class'] != "mpicbg.trakem2.transform.AffineModel2D":
                    logging.error("Wrong xml format!")
                    raise

            result.append((x_combined, y_combined, angle))

    return result


if __name__ == '__main__':
    test_filenames = ['data/mrhead01.xml', 'data/mrhead02.xml', 'data/mrhead03.xml', 'data/mrhead04.xml']
    print(parseXmlsWithTransformResults(test_filenames))
