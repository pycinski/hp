# The followng functions are from HistomicsTK library (https://digitalslidearchive.github.io/HistomicsTK)

import numpy as np
import collections

stain_color_map = {'dab': [0.27, 0.57, 0.78], 'eosin': [0.07, 0.99, 0.11], 'hematoxylin': [0.65, 0.7, 0.29], 'null': [0.0, 0.0, 0.0]}


def magnitude(m):
    """Get the magnitude of each column vector in a matrix"""
    return np.sqrt((m ** 2).sum(0))


def normalize(m):
    """Normalize each column vector in a matrix"""
    return m / magnitude(m)


def complement_stain_matrix(w):
    """Generates a complemented stain matrix
    Used to fill out empty columns of a stain matrix for use with
    color_deconvolution. Replaces right-most column with normalized
    cross-product of first two columns.

    Parameters
    ----------
    w : array_like
        A 3x3 stain calibration matrix with stain color vectors in columns.

    Returns
    -------
    w_comp : array_like
        A 3x3 complemented stain calibration matrix with a third
        orthogonal column.

    See Also
    --------
    histomicstk.preprocessing.color_deconvolution.color_deconvolution

    """

    stain0 = w[:, 0]
    stain1 = w[:, 1]
    stain2 = np.cross(stain0, stain1)
    # Normalize new vector to have unit norm
    return np.array([stain0, stain1, stain2 / np.linalg.norm(stain2)]).T


def sda_to_rgb(im_sda, I_0):
    """Transform input SDA image or matrix `im_sda` into RGB space.  This
    is the inverse of `rgb_to_sda` with respect to the first parameter

    Parameters
    ----------
    im_sda : array_like
        Image (MxNx3) or matrix (3xN) of pixels

    I_0 : float or array_like
        Background intensity, either per-channel or for all channels

    Note
    ----
    For compatibility purposes, passing I_0=None invokes the behavior of
    od_to_rgb.

    See Also
    --------
    histomicstk.preprocessing.color_conversion.rgb_to_sda,
    histomicstk.preprocessing.color_conversion.od_to_rgb,
    histomicstk.preprocessing.color_deconvolution.color_deconvolution,
    histomicstk.preprocessing.color_deconvolution.color_convolution

    """
    is_matrix = im_sda.ndim == 2
    if is_matrix:
        im_sda = im_sda.T

    od = I_0 is None
    if od:  # od_to_rgb compatibility
        I_0 = 256

    im_rgb = I_0 ** (1 - im_sda / 255.)
    return (im_rgb.T if is_matrix else im_rgb) - od


def rgb_to_sda(im_rgb, I_0, allow_negatives=False):
    """Transform input RGB image or matrix `im_rgb` into SDA (stain
    darkness) space for color deconvolution.

    Parameters
    ----------
    im_rgb : array_like
        Image (MxNx3) or matrix (3xN) of pixels

    I_0 : float or array_like
        Background intensity, either per-channel or for all channels

    allow_negatives : bool
        If False, would-be negative values in the output are clipped to 0

    Returns
    -------
    im_sda : array_like
        Shaped like `im_rgb`, with output values 0..255 where `im_rgb` >= 1

    Note
    ----
    For compatibility purposes, passing I_0=None invokes the behavior of
    rgb_to_od.

    See Also
    --------
    histomicstk.preprocessing.color_conversion.sda_to_rgb,
    histomicstk.preprocessing.color_conversion.rgb_to_od,
    histomicstk.preprocessing.color_deconvolution.color_deconvolution,
    histomicstk.preprocessing.color_deconvolution.color_convolution

    """
    is_matrix = im_rgb.ndim == 2
    if is_matrix:
        im_rgb = im_rgb.T

    if I_0 is None:  # rgb_to_od compatibility
        im_rgb = im_rgb.astype(float) + 1
        I_0 = 256

    if not allow_negatives:
        im_rgb = np.minimum(im_rgb, I_0)

    im_sda = -np.log(im_rgb/(1.*I_0)) * 255/np.log(I_0)
    return im_sda.T if is_matrix else im_sda


def convert_matrix_to_image(m, shape):
    """Convert a column matrix of pixels to a 3D image given by shape.
    The number of channels is taken from m, not shape.  If shape has
    length 2, the matrix is returned unchanged.  This is the inverse
    of convert_image_to_matrix:

    im == convert_matrix_to_image(convert_image_to_matrix(im),
    im.shape)

    """
    if len(shape) == 2:
        return m

    return m.T.reshape(shape[:-1] + (m.shape[0],))


def convert_image_to_matrix(im):
    """Convert an image (MxNx3 array) to a column matrix of pixels
    (3x(M*N)).  It will pass through a 2D array unchanged.

    """
    if im.ndim == 2:
        return im

    return im.reshape((-1, im.shape[-1])).T


def color_convolution(im_stains, w, I_0=None):
    """Perform Color Convolution.

    Reconstructs a color image from the stain matrix `w` and
    the individual images stored as channels in `im_stains` and generated
    by ColorDeconvolution.

    Parameters
    ----------
    im_stains : array_like
        An RGB image where in each channel contains image of one stain
    w : array_like
        A 3x3 matrix containing the stain colors in its columns.
        In the case of two stains, the third column is zero and will be
        complemented using cross-product. The matrix should contain a
        minumum two nonzero columns.
    I_0 : float or array_like, optional
        A float a 3-vector containing background RGB intensities.
        If unspecified, use the old OD conversion.

    Returns
    -------
    im_rgb : array_like
        Reconstructed RGB image with intensity values ranging from [0, 255],
        suitable for display.

    See Also
    --------
    histomicstk.preprocessing.color_deconvolution.complement_stain_matrix,
    histomicstk.preprocessing.color_deconvolution.color_deconvolution
    histomicstk.preprocessing.color_conversion.rgb_to_od
    histomicstk.preprocessing.color_conversion.od_to_rgb
    histomicstk.preprocessing.color_conversion.rgb_to_sda
    histomicstk.preprocessing.color_conversion.sda_to_rgb

    """
    # transform 3D input stain image to 2D stain matrix format
    m = convert_image_to_matrix(im_stains)

    # transform input stains to optical density values, convolve and
    # tfm back to stain
    sda_fwd = rgb_to_sda(m, 255 if I_0 is not None else None,
                                          allow_negatives=True)
    sda_conv = np.dot(w, sda_fwd)
    sda_inv = sda_to_rgb(sda_conv, I_0)

    # reshape output, transform type
    im_rgb = (convert_matrix_to_image(sda_inv, im_stains.shape)
              .clip(0, 255).astype(np.uint8))

    return im_rgb


def color_deconvolution(im_rgb, w, I_0=None):
    """Perform color deconvolution.

    The given RGB Image `I` is first first transformed into optical density
    space, and then projected onto the stain vectors in the columns of the
    3x3 stain matrix `W`.

    For deconvolving H&E stained image use:

    `w` = array([[0.650, 0.072, 0], [0.704, 0.990, 0], [0.286, 0.105, 0]])

    Parameters
    ----------
    im_rgb : array_like
        Input RGB Image that needs to be deconvolved.
    w : array_like
        A 3x3 matrix containing the color vectors in columns.
        For two stain images the third column is zero and will be
        complemented using cross-product. Atleast two of the three
        columns must be non-zero.
    I_0 : float or array_like, optional
        A float a 3-vector containing background RGB intensities.
        If unspecified, use the old OD conversion.

    Returns
    -------
    Stains : array_like
        An rgb image where in each channel contains the image of the
        stain of the corresponding column in the stain matrix `W`.
        The intensity range of each channel is [0, 255] suitable for
        displaying.
    StainsFloat : array_like
        An intensity image of deconvolved stains that is unbounded,
        suitable for reconstructing color images of deconvolved stains
        with color_convolution.
    Wc : array_like
        A 3x3 complemented stain matrix. Useful for color image
        reconstruction with color_convolution.

    See Also
    --------
    histomicstk.preprocessing.color_deconvolution.complement_stain_matrix,
    histomicstk.preprocessing.color_deconvolution.color_convolution
    histomicstk.preprocessing.color_conversion.rgb_to_od
    histomicstk.preprocessing.color_conversion.od_to_rgb
    histomicstk.preprocessing.color_conversion.rgb_to_sda
    histomicstk.preprocessing.color_conversion.sda_to_rgb

    """
    # complement stain matrix if needed
    if np.linalg.norm(w[:, 2]) <= 1e-16:
        wc = complement_stain_matrix(w)
    else:
        wc = w

    # normalize stains to unit-norm
    wc = normalize(wc)

    # invert stain matrix
    Q = np.linalg.inv(wc)

    # transform 3D input image to 2D RGB matrix format
    m = convert_image_to_matrix(im_rgb)[:3]

    # transform input RGB to optical density values and deconvolve,
    # tfm back to RGB
    sda_fwd = rgb_to_sda(m, I_0)
    sda_deconv = np.dot(Q, sda_fwd)
    sda_inv = sda_to_rgb(sda_deconv,  255 if I_0 is not None else None)

    # reshape output
    StainsFloat = convert_matrix_to_image(sda_inv, im_rgb.shape)

    # transform type
    Stains = StainsFloat.clip(0, 255).astype(np.uint8)

    # return
    Unmixed = collections.namedtuple('Unmixed',
                                     ['Stains', 'StainsFloat', 'Wc'])
    Output = Unmixed(Stains, StainsFloat, wc)

    return Output