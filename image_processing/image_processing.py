# import os

import file_reading as fr
import cv2
import numpy as np
from PIL import Image
from skimage import io
import subprocess
import logging
# import skimage.color.colorconv
import image_processing.htk


def add_alpha_to_image(filename, output_filename):
    mat = io.imread(filename)
    mean_mat = (np.mean(mat, axis=2)).astype(np.uint8)
    alpha = Image.fromarray(mean_mat, mode='L')
    image = Image.open(filename)
    image.putalpha(alpha)
    image.save(output_filename)


def add_alpha_to_image_based_on_hemato(filename, output_filename):
    alpha = Image.fromarray(separateChannelsHE(filename)[:, :, 0], mode='L')
    image = Image.open(filename)
    image.putalpha(alpha)
    image.save(output_filename)


def separateChannelsHE(input_image):

    stain_color_map = image_processing.htk.stain_color_map

    # specify stains of input image
    stains = ['hematoxylin',  # nuclei stain
              'eosin',  # cytoplasm stain
              'null']  # set to null if input contains only two stains

    # create stain matrix
    W = np.array([stain_color_map[st] for st in stains]).T

    try:
        image = io.imread(input_image)[:, :, :3]  # strip alpha channel, if present
    except ValueError as e:
        #image = cv2.imread(input_image, cv2.IMREAD_COLOR)
        logging.error("The image {} could not be read by skimage.io.\n try to convert it first, e.g.:".format(input_image))
        logging.error("convert {} -compression None {}".format(input_image, input_image))
        raise e

    #im_deconvolved = skimage.color.colorconv.rgb2hed(image)
    im_deconvolved = image_processing.htk.color_deconvolution(image, W)   # skimage.color.colorconv.rgb_from_hed)
    #cv2.imwrite(input_image+'-H.png', im_deconvolved[:, :, 0])
    #cv2.imwrite(input_image+'-E.png', im_deconvolved[:, :, 1])
    #cv2.imwrite(input_image+'-D.png', im_deconvolved[:, :, 2])
    return im_deconvolved.Stains


# noinspection PyPep8Naming
def runFijiTransformation(input_dir="", output_dir="", input_xml_dir="", fiji_exe="", script_file=""):
    logging.debug("run: runFijiTransformation({}, {}, {}, {}, {})".format(input_dir, output_dir, input_xml_dir, fiji_exe, script_file))
    FIJI_APP = fiji_exe or "/home/mba/programy/Fiji.app/ImageJ-linux64"
    # FIJI_APP = fiji_exe or "/home/bapy/share/Fiji.app/ImageJ-linux64"
    SCRIPT = script_file
    SOURCE_DIR = input_dir
    TARGET_DIR = output_dir
    TRANSF_DIR = input_xml_dir
    args = "source_dir='" + SOURCE_DIR + "',target_dir='" + TARGET_DIR + "',transf_dir='" + TRANSF_DIR + "'"
    print(SCRIPT, args)
    return subprocess.call([FIJI_APP,
                            "--ij2",
                            "--headless",
                            "--console",
                            "--run",
                            SCRIPT,
                            args
                            ])


# noinspection PyPep8Naming
def runFijiRegistration(input_dir="", output_dir="", output_xml_dir="", reference_image="", fiji_exe="",
                        script_file=""):
    logging.debug("run: runFijiRegistration({}, {}, {}, {}, {}, {})".format(input_dir, output_dir, output_xml_dir, reference_image, fiji_exe, script_file))
    FIJI_APP = fiji_exe or "/home/mba/programy/Fiji.app/ImageJ-linux64"
    # FIJI_APP = fiji_exe or "/home/bapy/share/Fiji.app/ImageJ-linux64"
    # source directory
    SCRIPT = script_file or "scripts/register-images.py"
    PREFIX = "/home/bapy/projects/python/hp/"
    SOURCE_DIR = input_dir or PREFIX + "data/mrhead/orig/"
    # output directory
    TARGET_DIR = output_dir or PREFIX + "data/mrhead/out/"
    # transforms directory
    TRANSF_DIR = output_xml_dir or PREFIX + "data/mrhead/out/"
    # reference image
    REFERENCE_NAME = reference_image or "mrhead01.png"

    if not SOURCE_DIR.endswith('/'):
        SOURCE_DIR += '/'
    if not TARGET_DIR.endswith('/'):
        TARGET_DIR += '/'
    if not TRANSF_DIR.endswith('/'):
        TRANSF_DIR += '/'

    # Python 3.5:   '"key=val"'
    # args = '"' + "source_dir="+SOURCE_DIR+",target_dir="+TARGET_DIR+",transf_dir="+TRANSF_DIR+",reference_name="+REFERENCE_NAME + '"'
    # Python 3.6:   'key="val"'
    args = "source_dir='" + SOURCE_DIR + "',target_dir='" + TARGET_DIR + "',transf_dir='" + TRANSF_DIR + "',reference_name='" + REFERENCE_NAME + "'"
    print(SCRIPT, args)
    popen = subprocess.Popen([FIJI_APP,
                            "--ij2",
                            "--headless",
                            "--console",
                            "--run",
                            SCRIPT,
                            args
                            ], stdout=subprocess.PIPE)

    # WARNING Dirty hach. The above FIJI_APP could not finish after performing registration, so let's terminate this
    # function ass soon an the registration finishes.
    for stdout_line in iter(popen.stdout.readline, ""):
        print(stdout_line)
        if stdout_line.decode('ascii').startswith("END_OF_FIJI_STACK_PROCESSING"):
            popen.stdout.close()
            return 0

    raise RuntimeError("Fiji could not end its work.")


def transform_images_by_fiji(config, scale, is_cropped):
    #TODO create folders "-tr" and remove it at cleaning stage
    input_dir = ''  # define here just to avoid warnings
    if is_cropped:
        input_dir = config.base_dir + config.output_dir + config.cropped_subdir + str(scale) + "/"
        output_dir = config.base_dir + config.output_dir + config.cropped_registered_fiji_subdir + str(scale) + config.transformed_channel_dir_suffix
    else:
        if scale == 1:
            input_dir = config.base_dir + config.original_subdir
        else:
            input_dir = config.base_dir + config.resized_subdir + str(scale) + "/"
        output_dir = config.base_dir + config.output_dir + config.whole_registered_fiji_subdir + str(scale) + config.transformed_channel_dir_suffix
    input_xml_dir = output_dir[:-1-len(config.transformed_channel_dir_suffix)+1] + '-h/'
    return_code = runFijiTransformation(input_dir,
                                        output_dir,
                                        input_xml_dir,
                                        config.fiji_exe,
                                        config.script_file_transform)
    if return_code != 0:
        logging.error("Fiji transformation failed")
        raise RuntimeError("Fiji transformation failed")


def register_images_by_fiji(config, scale, is_cropped, channel=None, use_previous_results=False):
    input_dir = ''  # define here just to avoid warnings
    if is_cropped:
        if scale == 1 or scale == 2 or scale == 4 or scale == 8 or scale == 16 or scale == 32 or scale == 64:
            input_dir = config.base_dir + config.output_dir + config.cropped_subdir + str(scale) + "/"
        else:
            raise ValueError("Current data-flow allows only cropped between x64 and and x1")
        output_dir = config.base_dir + config.output_dir + config.cropped_registered_fiji_subdir + str(scale) + "/"
        try:
            # if the channel has not been set, then register rgb images directly
            if channel.lower() == "h" or channel.lower().startswith("hema"):
                input_dir = input_dir[:-1] + config.hemato_channel_dir_suffix
                output_dir = output_dir[:-1] + config.hemato_channel_dir_suffix
            elif channel.lower() == "e" or channel.lower().startswith("eos"):
                input_dir = input_dir[:-1] + config.eosine_channel_dir_suffix
                output_dir = output_dir[:-1] + config.eosine_channel_dir_suffix
        except AttributeError:
            pass

    else:
        if scale == 1:
            input_dir = config.base_dir + config.original_subdir
        elif scale == 2 or scale == 4 or scale == 8 or scale == 16 or scale == 32 or scale == 64:
            input_dir = config.base_dir + config.resized_subdir + str(scale) + "/"
        output_dir = config.base_dir + config.output_dir + config.whole_registered_fiji_subdir + str(scale) + "/"

    output_xml_dir = output_dir

    logging.info("Run Fiji with full images " + str(scale))
    if use_previous_results:
        logging.warning("Using previous results of Fiji registration")
    else:
        return_code = runFijiRegistration(input_dir,
                                          output_dir,
                                          output_xml_dir,
                                          config.reference_image,
                                          config.fiji_exe,
                                          config.script_file_register)
        if return_code != 0:
            logging.error("Fiji registration failed")
            raise RuntimeError("Fiji registration failed")

    xmlFilenames = fr.getFilesFromDir(output_xml_dir, "*.xml")
    if use_previous_results and len(xmlFilenames) == 0:
        logging.error("Could not use previous results. Re-run full method!")
        raise ValueError("Could not use previous results. Re-run full method!")
    return fr.parseXmlsWithTransformResults(xmlFilenames)


def postprocess_final_images(direcory, pattern):
    # TODO implement or remove
    file_list = fr.getFilesFromDir(direcory, pattern)
    for file in file_list:
        pass
        # image_data = cv2.imread(file)


def deconvolute_and_save_cropped_images(config, input_directory, image_filenames):
    logging.warning("run: deconvolute_and_save_cropped_images({}, {}, {})".format(config, input_directory, image_filenames))
    for file in image_filenames:
        image_filename = input_directory + '/' + file
        deconvoluted_image_data = separateChannelsHE(image_filename)
        hemato_channel = deconvoluted_image_data[:, :, 0]
        eosine_channel = deconvoluted_image_data[:, :, 1]
        residual_channel = deconvoluted_image_data[:, :, 2]
        # remove trailing "/" from the directory path
        if input_directory.endswith('/'):
            input_directory = input_directory[:-1]
        cv2.imwrite(input_directory + config.hemato_channel_dir_suffix + file, hemato_channel)
        cv2.imwrite(input_directory + config.eosine_channel_dir_suffix + file, eosine_channel)
        cv2.imwrite(input_directory + config.residual_channel_dir_suffix + file, residual_channel)


if __name__ == "__main__":
    pass
    #test_file = "/media/bapy/Samsung_T5/hp/adenoca/resized-registered-left-roi/1-full-ROI/DS01.tif.png.tif"
    #out_file = "/media/bapy/Samsung_T5/hp/adenoca/resized-registered-left-roi/1-full-ROI/DS01-tmp.png"
    #inputdir = "/media/bapy/Samsung_T5/hp/adenoca/resized-registered-left-roi/1-full-ROI"
    #filelist = fr.getFilesFromDir(inputdir, "*.tif")
    # add_alpha_to_image_based_on_hemato(test_file, out_file)
    #for file in filelist:
    #    add_alpha_to_image_based_on_hemato(file, file + '-rgba-h.png')
    #    # add_alpha_to_image(file, file+'-rgba-mean.png')
