# Volumetric registration of stacks of histopathology RGB images

A repository for volumetric registration of histopathology images.

The code was employed for a publication submitted to the ITIB'20 Conference (https://itib.polsl.pl). The detailed description of the algorithm has been presented in the publication. Bibliometric reference of the publication is listed below. The newest parts of the code is founded by the Silesian Univ. Tech. grant no. 07/010/BKM20/0049.

## Run instructions

Unfortunately, the original images are not publicly available.

The code was developed for personal purpose, therefore lots of input parameters (eg. file paths, region coords) are hard-coded. There may occure words in Polish language. 

Most of the input parameters are defined as members of `Config` class defined in [project_config.py](project_config/project_config.py) file. The most non-obvious are:

- `roi_scale_64`  x-y coordinates of the ROI corners of the first image (ie. the reference image of a stack), downscaled to the resolution 1/64
- `base_dir` a path (either an absolute path or a path relative to the working directory)
- `fiji_exe` a path to the FIJI executable
- `platform.node()`-related magic names (`i7559` and `icod`) are the names of my two computers

Coordinates of the ROI is overwritten at the beginning of [main.py](main.py). The coordinates of ROI (rescaled to the image with a factor 1/2) are given as e.g. `roiL_x2` and `roiL_y2`. Next, these corrds are recalculated to the scale 1/64.

## Input files and dirs tree

- For a three-slices stack the following files are expected to exist in `base_dir`. A script [resize.sh](scripts/resize.sh) may be used to generate downsampled images:
```
data
├── original
│   ├── DS01.tif
│   ├── DS02.tif
│   └── DS03.tif
└── resized
    ├── 16
    │   ├── DS01.tif
    │   ├── DS02.tif
    │   └── DS03.tif
    ├── 2
    │   ├── DS01.tif
    │   ├── DS02.tif
    │   └── DS03.tif
    ├── 32
    │   ├── DS01.tif
    │   ├── DS02.tif
    │   └── DS03.tif
    ├── 4
    │   ├── DS01.tif
    │   ├── DS02.tif
    │   └── DS03.tif
    ├── 64
    │   ├── DS01.tif
    │   ├── DS02.tif
    │   └── DS03.tif
    └── 8
        ├── DS01.tif
        ├── DS02.tif
        └── DS03.tif
```

Final output structure of directories is the following:
```
results
├── cropped
│     ├── 1
│     ├── 16
│     ├── 1-e
│     ├── 1-h
│     └── 1-r
├── resized-cropped-registered
│     ├── 1
│     ├── 16
│     ├── 1-e
│     ├── 1-final
│     ├── 1-final-e
│     ├── 1-final-h
│     ├── 1-final-r
│     ├── 1-h
│     └── 1-r
└── resized-whole-registered
      └── 64
```

- It is assumed, that patterns of the file names are `DS%02d.tif`. If the pattern should be different, all hard-coded occurrences of `DS01.tif` should be replaced.
- The postfixes `-e`, `-h` and `-r` denotes the images after deconvolution: eosine, hematoxyline and dab, respectively.

## Environment
The code runs with Python 3.7.6 via [PyEnv](https://github.com/pyenv/pyenv) on a Linux machine.

## Reference

The publication can be found by the following reference:

    Bartłomiej Pyciński, Yukako Yagi, Ann E. Walts and Arkadiusz Gertych. "3-D Tissue
    Image Reconstruction from Digitized Serial Histologic Sections to Visualize Small
    Tumor Nests in Lung Adenocarcinomas”. W: Information Technology in Biomedicine. 
    V. 1186. Advances in Intelligent Systems and Computing. Springer International 
    Publishing, 2020, pp. 55–70. doi: 10.1007/978-3-030-49666-1_5

## Acknowledgements
The most recent parts of the code have been developed within the Silesian University of Technology grant no. 07/010/BKM20/0049.

## License
The code is licensed under the CC BY 4.0 license.

© 2019-2021 Bartłomiej Pyciński, Silesian University of Technology, Poland

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)
