import logging
import os
import sys
import re
import shutil
import project_config

import image_processing as ip

import file_reading as fr


def move_files_into_subdirectories(path, pattern="*"):
    if not os.path.isdir(path):
        logging.error("Wrong path: {}".format(path))
        raise FileNotFoundError("Wrong path: {}".format(path))
    files = fr.getFilesFromDir(path, pattern)
    number_of_dirs = len([file for file in files if os.path.isdir(file)])
    number_of_files = len([file for file in files if os.path.isfile(file)])
    if number_of_dirs > 0 and number_of_files == 0:
        logging.info("No need to modify files in directory {}".format(path))
        return
    elif number_of_dirs == 0 and number_of_files == 0:
        logging.warning("Empty directory: {}".format(path))
        return
    elif number_of_dirs > 0 and number_of_files > 0:
        logging.warning("The directory contains both files and directories. Skipping: {}".format(path))
        return
    elif number_of_dirs == 0 and number_of_files > 0:
        for file_ in files:
            dir_name = os.path.dirname(file_)
            file_name = os.path.basename(file_)
            pattern = r'^\w+\d+_([0-9]).*$'
            r1 = re.match(pattern, file_name)
            if r1:
                subdir_name = path + '/' + r1.groups()[0]
                if not os.path.exists(subdir_name):
                    os.makedirs(subdir_name)
                logging.info("move {} -> {}".format(file_, subdir_name))
                shutil.move(file_, subdir_name)
    else:
        raise RuntimeError()


def preprocess_directories(path_names):
    if type(path_names) is str:
        path_names = [path_names]
    for path in path_names:
        for dir_name in sorted(os.listdir(path)):
            move_files_into_subdirectories(path+'/'+dir_name)


#config = {}


def get_list_of_directories_with_images_to_register(basedir):
    result = []
    for root_dir in fr.getFilesFromDir(basedir):
        for roi_dir in fr.getFilesFromDir(root_dir):
            for input_normalized_dir in fr.getFilesFromDir(roi_dir):
                # append only directories which names end with a digit
                if re.search(r'\d$', input_normalized_dir):
                    result.append(input_normalized_dir)
    return result

def create_missing_output_directories(input_directories):
    for directory in input_directories:
        directory = directory.rstrip('/')
        try:
            os.mkdir(directory+'-h-registered')
        except FileExistsError: pass
        try:
            os.mkdir(directory+'-registered')
        except FileExistsError: pass
        try:
            os.mkdir(directory+'-transformed')
        except FileExistsError: pass
        try:
            os.mkdir(directory+'-h')
        except FileExistsError: pass
        try:
            os.mkdir(directory+'-e')
        except FileExistsError: pass
        try:
            os.mkdir(directory+'-r')
        except FileExistsError: pass


def register_normalized_images_from_base_directory(config, basedir):
    logging.basicConfig(level=logging.DEBUG)
    # do it only once!
    # preprocess_directories(fr.getFilesFromDir(basedir))
    input_directories = get_list_of_directories_with_images_to_register(basedir)
    # do it only once!
    # create_missing_output_directories(input_directories)
    for lp, input_directory in enumerate(input_directories, start=1):
        input_directory = input_directory.rstrip('/')
        file_list = sorted(os.listdir(input_directory))
        print (input_directory, file_list)
        ip.deconvolute_and_save_cropped_images(config, input_directory, file_list)
        # basename of the reference image is the same for all registration methods
        reference_image = os.path.basename(fr.getFilesFromDir(input_directory)[0])
        # registration of RGB image
        return_code = ip.runFijiRegistration(input_directory,
                                             input_directory+'-registered',
                                             input_directory+'-registered',
                                             reference_image,
                                             config.fiji_exe,
                                             config.script_file_register)
        if return_code != 0:
            logging.error("Fiji registration failed")
            raise RuntimeError("Fiji registration failed")
        # registration of Hematoxylin image
        return_code = ip.runFijiRegistration(input_directory+'-h',
                                             input_directory+'-h-registered',
                                             input_directory+'-h-registered',
                                             reference_image,
                                             config.fiji_exe,
                                             config.script_file_register)
        if return_code != 0:
            logging.error("Fiji registration failed")
            raise RuntimeError("Fiji registration failed")
        # Apply registration results of Hemato to RGB image
        return_code = ip.runFijiTransformation(input_directory+'/',
                                               input_directory+'-transformed'+'/',
                                               input_directory+'-h-registered/',
                                               config.fiji_exe,
                                               config.script_file_transform)
        if return_code != 0:
            logging.error("Fiji transformation failed")
            raise RuntimeError("Fiji transformation failed")

        for filename in fr.getFilesFromDir(input_directory+'-transformed'+'/', "*.tif"):
            # TODO change basename of final images
            ip.add_alpha_to_image(filename, filename + '-rgba.tif')
            ip.add_alpha_to_image_based_on_hemato(filename, filename + '-rgba-h.tif')

        logging.info("Processed {} of {}.".format(lp, len(input_directories)))
        #sys.exit(1)


if __name__ == "__main__":
    config = project_config.Config()
    basedir = '/media/mba/Samsung_T5/hp/adenoca/color-normalisation'
    basedir = '/home/bapy/data/color-normalized/before_normalisation'
    basedir = '/home/bapy/data/color-normalized/results'
    basedir = '/home/bapy/data/color-normalized/normalized'

    register_normalized_images_from_base_directory(config, basedir)
