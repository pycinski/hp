import itertools
import math
import os
import sys
import numpy as np
from PIL import Image
from skimage import io
import scipy

import file_reading as fr
import image_processing as ip
import transformations as tr
import logging
import numpy as np
import cv2

def jaccard(im1, im2):
    im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)
    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")
    intersection = np.logical_and(im1, im2)
    union = np.logical_or(im1, im2)
    return intersection.sum() / float(union.sum())

# inputdir = '/media/bapy/Samsung_T5/hp/adenoca/resized-registered-right-roi/1-full-ROI'
# inputdir = '/home/mba/Dokumenty/hp/roi-1/1-registered'

inputbasedir='/home/bapy/dell/hp_data/data/color-normalized/results/Color_normalized_1'


class conf:
    hemato_channel_dir_suffix = '/hemato-'


for threshold_level in range(255, -1, -1):
    print (threshold_level)
    for roi_nr in [1, 2, 3]:
        roidir = inputbasedir + f"/roi-{roi_nr}"
        for i in range(0, 7+1):
            inputdir = roidir + f'/{i}-registered/'
            #inputdir = "/home/bapy/data/adenoca/results/left-roi-again/resized-cropped-registered/16"
            #print(inputdir)
            files_all = fr.getFilesFromDir(inputdir, "*.tif")
            #print(files_all)

            # obrazy oryginalne:
            files = [f for f in files_all if not (('-result' in f) or ('-new' in f) or ('hemato' in f))]
            ## obrazy hemato
            #files = files_all
            #files = [f for f in files_all if '-crop.tif' in f]
            ## Te trzy linijki do zrobienia dekonwolucji
            #files = [os.path.basename(f) for f in files_all if 'hemato' in f]
            #ip.deconvolute_and_save_cropped_images(conf, inputdir, files)
            #continue

            prev_file = files[0]
            prev_im = fr.openImageFile(prev_file)
            #prev_imcv = cv2.cvtColor(np.array(prev_im), cv2.COLOR_RGB2HSV)[:,:,2]
            prev_imcv = cv2.cvtColor(np.array(prev_im), cv2.COLOR_RGB2GRAY)
            #prev_imcv = np.array(prev_im).mean(axis=2).astype('uint8')
            #prev_imcv = np.array(prev_im)[:,:,0]
            #prev_imcv = np.array(prev_im)

            #prev_val, prev_retim = cv2.threshold(prev_imcv, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            prev_val, prev_retim = cv2.threshold(prev_imcv, threshold_level, 255, cv2.THRESH_BINARY)
            prev_retim = np.logical_not(prev_retim)

            results = []
            for file in files[1:]:
                cur_im = fr.openImageFile(file)
                #cur_imcv = cv2.cvtColor(np.array(cur_im), cv2.COLOR_RGB2HSV)[:,:,2]
                cur_imcv = cv2.cvtColor(np.array(cur_im), cv2.COLOR_RGB2GRAY)
                #cur_imcv = np.array(cur_im).mean(axis=2).astype('uint8')
                #cur_imcv = np.array(cur_im)[:, :, 0]
                #cur_imcv = np.array(cur_im)


                #cur_val, cur_retim = cv2.threshold(cur_imcv, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
                cur_val, cur_retim = cv2.threshold(cur_imcv, threshold_level, 255, cv2.THRESH_BINARY)
                cur_retim = np.logical_not(cur_retim)
                # cur_val, cur_retim = cv2.threshold(cur_imcv, 250, 255, cv2.THRESH_BINARY)

                jac = 1 - scipy.spatial.distance.jaccard(cur_retim.ravel(), prev_retim.ravel())

                #merged = (prev_retim.astype(int) + cur_retim.astype(int)) * 127+1
                #cv2.imwrite(file+'-result.tif', merged)

                #mergarray = merged.ravel()
                #print(jac)
                #print (len(mergarray), len(mergarray)-np.count_nonzero(mergarray-254), len(mergarray)-np.count_nonzero(mergarray-127))
                #print (mergarray.min(), mergarray.max(), mergarray.std(), mergarray.mean())

                results.append(jac)
                prev_val, prev_retim = cur_val, cur_retim

            print(f"ROI-{roi_nr} color {i}: {np.mean(results):.4f},{np.std(results):.4f}")
            #for val in results:
            #    print(val, end=' ')
            #print('')

            #raise RuntimeError()

