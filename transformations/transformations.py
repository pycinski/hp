import itertools
import logging
import sys

import numpy as np


# noinspection PyPep8Naming
def computeBoundingBoxes(p1, p2, p3, p4, transformations):
    """
    :param p1: a point (x,y)
    :param p2: a point (x,y)
    :param p3: a point (x,y)
    :param p4: a point (x,y)
    :param transformations: a list of tuples with rigid body transformation parameters
    (translation_x, translation_y, angle)
    :return: a tuple of new bounding boxes (x_min, y_min, x_max, y_max)
    """
    result = []
    for transformation in transformations:
        result.append(computeBoundingBox(p1, p2, p3, p4, transformation))
    return result


# noinspection PyPep8Naming
def computeOneBoundingBoxFromROI(roi: list) -> tuple:
    """
    :param roi: list of tuples ((x0,y0), (x1,y1), (x2,y2), (x3,y3))
    :return: one tuple (x_min, y_min, x_max, y_max) with coordination of one bounding box
    """
    result = np.mat(roi).T
    x_min, y_min = np.min(result, axis=1)
    x_max, y_max = np.max(result, axis=1)
    return float(x_min), float(y_min), float(x_max), float(y_max)


# noinspection PyPep8Naming
def transformROI(p1, p2, p3, p4, transformation):
    p1 = transformPoint(p1, transformation)
    p2 = transformPoint(p2, transformation)
    p3 = transformPoint(p3, transformation)
    p4 = transformPoint(p4, transformation)
    return p1, p2, p3, p4


# noinspection PyPep8Naming
def computeBoundingBox(p1, p2, p3, p4, transformation):
    """
    Computes bounding box after applying a rigid-body transformation on four points.
    :param p1: a point (x,y)
    :param p2: a point (x,y)
    :param p3: a point (x,y)
    :param p4: a point (x,y)
    :param transformation: a tuple with rigid body transformation parameters
    (translation_x, translation_y, angle)
    :return: a tuple of new bounding box (x_min, y_min, x_max, y_max)
    """

    ul, ur, ll, lr = transformROI(p1, p2, p3, p4, transformation)

    # ul = transformPoint(p1, transformation)
    # ur = transformPoint(p2, transformation)
    # ll = transformPoint(p3, transformation)
    # lr = transformPoint(p4, transformation)

    x_min = int(np.min((ul[0], ur[0], ll[0], lr[0])))
    x_max = int(np.max((ul[0], ur[0], ll[0], lr[0])))
    y_min = int(np.min((ul[1], ur[1], ll[1], lr[1])))
    y_max = int(np.max((ul[1], ur[1], ll[1], lr[1])))

    return x_min, y_min, x_max, y_max


# noinspection PyPep8Naming
def transformPoint(p1, transformation):
    """
    :param p1: a point (x,y)
    :param transformation: a tuple with rigid body transformation parameters
    (translation_x, translation_y, angle)
    :return: new point coordinates [x,y]
    """

    tx, ty, alpha = transformation

    rotation = np.eye(2)
    if alpha != 0:
        rotation = np.mat([[np.cos(alpha), -np.sin(alpha)], [np.sin(alpha), np.cos(alpha)]])
    p_transformed = rotation * np.mat(p1).T + np.mat([[tx], [ty]])
    return p_transformed.squeeze().tolist()[0]


# noinspection PyPep8Naming
def inverseTransformation(transformation):
    """
    Compute inverse transformation assuming it is a rigid-body one (only translation and rotation)
    :param transformation: a tuple with rigid body transformation parameters
    (translation_x, translation_y, angle)
    :return: inverse transform (translation_x, translation_y, angle)
    """

    tx, ty, alpha = transformation
    ret_tx = -np.cos(alpha)*tx - np.sin(alpha)*ty
    ret_ty = np.sin(alpha)*tx - np.cos(alpha)*ty
    ret_alpha = -alpha
    return ret_tx, ret_ty, ret_alpha


# noinspection PyPep8Naming
def scaleRoiByFactor(roi, factor):
    # TODO do not forget about casting between int and float
    coords = np.mat(roi).T
    center = np.mean(coords, axis=1)
    new_roi = []
    for point in roi:
        diff = np.mat(point).T-center
        new_point = center + diff*factor
        new_roi.append(*new_point.T.tolist())
    return new_roi


# noinspection PyPep8Naming
def scaleBBByFactor(bb, factor):
    # TODO do not forget about casting between int and float
    bb = np.array(bb)
    center = np.mean(np.resize(bb, (2, 2)), axis=0)
    diff = center-bb[:2]
    result = np.append(center-diff*factor, center+diff*factor)
    return result


if __name__ == '__main__':
    test_transformations = ((2.676041245579707E-18, -1.0000000000000555, 0.0),
                            (-1.2015967466571188E-6, 82.99074449430657, 100.00176472707602),
                            (-0.2618097477875165, -83.21037111478725, 152.13623516972422)
                            )
    for test_transformation in test_transformations:
        print(computeBoundingBox(*(itertools.product((0, 977), (0, 891))), test_transformation))
