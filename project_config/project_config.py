# noinspection PyPep8Naming
import logging
import os
import shutil
import platform

class Config:
    def __init__(self):
        self.roi_scale_64 = ((0, 0), (0, 0), (0, 0), (0, 0))
        self.base_dir = "/media/bapy/Samsung_T5/hp/adenoca/"
        self.output_dir = "results/"
        self.cropped_subdir = "cropped/"
        self.resized_subdir = "resized/"
        self.original_subdir = "original/"
        self.cropped_registered_fiji_subdir = "resized-cropped-registered/"
        self.whole_registered_fiji_subdir = "resized-whole-registered/"
        self.hemato_channel_dir_suffix = "-h/"
        self.eosine_channel_dir_suffix = "-e/"
        self.residual_channel_dir_suffix = "-r/"
        self.transformed_channel_dir_suffix = "-final/"
        self.use_previous_results = False
        self.skip_saving = False
        self.reference_image = "DS01.tif"
        #i7559
        if platform.node() == 'i7559':
            self.fiji_exe = "/home/bapy/usr/share/Fiji.app/ImageJ-linux64"
        elif platform.node() == 'icod':
            self.fiji_exe = "/home/bapy/share/Fiji.app/ImageJ-linux64"
        else:
            raise RuntimeError('Check your environment.')
        self.script_file_register = "scripts/register-images.py"
        self.script_file_transform = "scripts/transform-images.py"

        self.roi_coeficient_x16 = 4  # How many times this ROI is bigger at level x 16 than original one
        self.decrease_param = 1.0  # should we decrease final ROI more
        self._test_and_fix_dirnames()

    def clean_output_tree(self):
        try:
            #TODO add sth like realpath
            shutil.rmtree(self.base_dir + self.output_dir)
        except FileNotFoundError:
            pass
        self.create_output_tree()

    def create_output_tree(self):
        for scale in [1, 16]:
            try:
                os.makedirs(self.base_dir + self.output_dir + self.cropped_subdir + str(scale))
            except FileExistsError:
                pass
            try:
                os.makedirs(self.base_dir + self.output_dir + self.cropped_registered_fiji_subdir + str(scale))
            except FileExistsError:
                pass
        for scale in [1]:
            try:
                os.makedirs(self.base_dir + self.output_dir + self.cropped_subdir + str(scale) + self.eosine_channel_dir_suffix)
                os.makedirs(self.base_dir + self.output_dir + self.cropped_subdir + str(scale) + self.hemato_channel_dir_suffix)
                os.makedirs(self.base_dir + self.output_dir + self.cropped_subdir + str(scale) + self.residual_channel_dir_suffix)
                #os.makedirs(self.base_dir + self.output_dir + self.cropped_subdir + str(scale) + self.transformed_channel_dir_suffix)
            except FileExistsError:
                # TODO: Warn message
                pass
            try:
                os.makedirs(self.base_dir + self.output_dir + self.cropped_registered_fiji_subdir + str(scale) + self.eosine_channel_dir_suffix)
                os.makedirs(self.base_dir + self.output_dir + self.cropped_registered_fiji_subdir + str(scale) + self.hemato_channel_dir_suffix)
                os.makedirs(self.base_dir + self.output_dir + self.cropped_registered_fiji_subdir + str(scale) + self.residual_channel_dir_suffix)
                os.makedirs(self.base_dir + self.output_dir + self.cropped_registered_fiji_subdir + str(scale) + self.transformed_channel_dir_suffix)
                os.makedirs(self.base_dir + self.output_dir + self.cropped_registered_fiji_subdir + str(scale) + self.transformed_channel_dir_suffix[:-1] + self.eosine_channel_dir_suffix)
                os.makedirs(self.base_dir + self.output_dir + self.cropped_registered_fiji_subdir + str(scale) + self.transformed_channel_dir_suffix[:-1] + self.hemato_channel_dir_suffix)
                os.makedirs(self.base_dir + self.output_dir + self.cropped_registered_fiji_subdir + str(scale) + self.transformed_channel_dir_suffix[:-1] + self.residual_channel_dir_suffix)
            except FileExistsError:
                # TODO: Warn message
                pass
        for scale in [64]:
            try:
                os.makedirs(self.base_dir + self.output_dir + self.whole_registered_fiji_subdir + str(scale))
            except FileExistsError:
                # TODO: Warn message
                pass

    def set_and_create_new_output_tree(self, dirname: str):
        if len(dirname) == 0:
            raise ValueError('Name of output directory cannot be empty')
        self.output_dir = dirname
        if not self.output_dir.endswith('/'):
            self.output_dir += '/'

        self.create_output_tree()

    @staticmethod
    def set_logging():
        logging.basicConfig(level=logging.DEBUG)

    def _test_and_fix_dirnames(self):
        if not self.base_dir.endswith('/'):
            self.base_dir += '/'
        if not self.output_dir.endswith('/'):
            self.output_dir += '/'
        if not self.cropped_subdir.endswith('/'):
            self.cropped_subdir += '/'
        if not self.resized_subdir.endswith('/'):
            self.resized_subdir += '/'
        if not self.original_subdir.endswith('/'):
            self.original_subdir += '/'
        if not self.cropped_registered_fiji_subdir.endswith('/'):
            self.cropped_registered_fiji_subdir += '/'
        if not self.whole_registered_fiji_subdir.endswith('/'):
            self.whole_registered_fiji_subdir += '/'
        if not self.hemato_channel_dir_suffix.endswith('/'):
            self.hemato_channel_dir_suffix += '/'
        if not self.eosine_channel_dir_suffix.endswith('/'):
            self.eosine_channel_dir_suffix += '/'
        if not self.residual_channel_dir_suffix.endswith('/'):
            self.residual_channel_dir_suffix += '/'


