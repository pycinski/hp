cd ~/data/adenoca/original
convert   DS01.tif -fill none -stroke blue -strokewidth 6 -draw "rectangle 1316,1820 1448,1952 rectangle 996,2116 1128,2232 rectangle 1000,1064 1096,1192" kwadraty-b.png
convert   DS01.tif -fill none -stroke green -strokewidth 6 -draw "rectangle 1316,1820 1448,1952 rectangle 996,2116 1128,2232 rectangle 1000,1064 1096,1192" kwadraty.png


#wycięcie fragmentu obrazu - współrzędne zapisane w Excelu
convert DS01.tif  -crop  5280x4640+14352+32464   crop-roi2-25.png





# przygotowanie obrazków pod fig itib

convert DS01-1x1.tif -crop 4224x3712+14880+32928 crop1x1-wiekszy.png
convert crop1x1-wiekszy.png -rotate "7%" crop1x1-rotated.png
identify crop1x1-rotated.png
# 4646x4202
#left = (4646-2112)/2
#upper = (4202-1856)/2
# jakieś problemy z geometrią obrazu, stąd takie dalsze bezsensowne przekształcenia
convert crop1x1-rotated.png crop1x1-rotated.png.tif
convert crop1x1-rotated.png.tif crop1x1-rotated.png.tif.png
identify crop1x1-rotated.png.tif.png 
# 4646x4202 4646x4202+14669+32683
convert crop1x1-rotated.png.tif.png  -crop 2112x1856+$((14669+1267))+$((32683+1173)) crop1x1-final.png


convert crop1x1-wiekszy.png -rotate "-7%" crop1x1-rotated-.png
identify crop1x1-rotated-.png
# 4646x4202
#left = (4646-2112)/2
#upper = (4202-1856)/2
# jakieś problemy z geometrią obrazu, stąd takie dalsze bezsensowne przekształcenia
convert crop1x1-rotated-.png crop1x1-rotated-.png.tif
convert crop1x1-rotated-.png.tif crop1x1-rotated-.png.tif.png
identify crop1x1-rotated-.png.tif.png 
# 4646x4202 4646x4202+14669+32683
convert crop1x1-rotated-.png.tif.png  -crop 2112x1856+$((14669+1267))+$((32683+1173)) crop1x1-final-.png



convert DS01-1x16.tif -crop 660x580+732+1884   crop1x16-wiekszy.png
convert crop1x16-wiekszy.png -rotate "7%" crop1x16-rotated.png
convert crop1x16-rotated.png crop1x16-rotated.png.tif
convert crop1x16-rotated.png.tif crop1x16-rotated.png.tif.png
identify crop1x16-rotated.png.tif.png 
convert crop1x16-rotated.png.tif.png  -crop 330x290+$((698+200))+$((1845+184))  crop1x16-final.png

convert crop1x16-wiekszy.png -rotate "-7%" crop1x16-rotated-.png
convert crop1x16-rotated-.png crop1x16-rotated-.png.tif
convert crop1x16-rotated-.png.tif crop1x16-rotated-.png.tif.png
identify crop1x16-rotated-.png.tif.png 
convert crop1x16-rotated-.png.tif.png  -crop 330x290+$((698+200))+$((1845+184))  crop1x16-final-.png







# montaż różnych normalizacji
BASEDIR=~/dell/hp_data/data/color-normalized/results/Color_normalized_1/roi-3
cp ~/data/color-normalized/before_normalisation/original/roi-3/1-transformed-cropped/DS35.tif $BASEDIR
cd $BASEDIR
cp -i *transformed/*DS35* .
montage DS35.tif  *_*tif -geometry +80+80 colors.png
convert colors.png -resize "50%" colors50.png 




